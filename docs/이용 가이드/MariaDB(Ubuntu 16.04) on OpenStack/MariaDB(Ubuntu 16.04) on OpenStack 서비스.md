## 개요(OverView)

> 전자정부 클라우드 플랫폼에서 제공하는 MariaDB(Ubuntu 16.04) on OpenStack 서비스입니다.  
> MariaDB는 MySQL을 대체할 수 있는 오픈 소스 기반의 데이터베이스 관리 시스템(RDBMS)입니다.  
> 오픈스택 기반의 가상머신을 통해 서비스를 제공하므로 관리가 편리하여 원활한 운영이 가능합니다.  
 
<br>

---

## 사용하기 전에

**Q. 본 서비스를 이용하면 어떤 이점이 있나요?**  
- 빠르고 쉽게 MariaDB를 이용하실 수 있으며, 콘솔을 통해 사용자가 원하는 설정을 간편하게 세팅할 수 있습니다.  

<br>

**Q. 본 서비스의 이용 절차는 어떻게 되나요?**  
- **서비스 이용 신청** > **서비스 가상머신에 유동아이피 연결** > **서비스 접속 및 DB 연결** 순으로 진행됩니다.  

<br>

---

## MariaDB(Ubuntu 16.04) on OpenStack 서비스 이용가이드

### 1. 서비스 이용 절차

#### 1-1. 서비스 신청하기

**1\) 사용자 포털 로그인**  

![login](img/mariadb_login.png)

- [사용자 포털](https://www.dev.egovp.kr/eGovPlatform/user/app/user/login)에 접속 및 로그인합니다.  

<br><br>

**2\) 서비스 상세 페이지로 이동**  

![topMenu](img/mariadb_top_menu.png)

- 상단 메뉴 중 **서비스** > **공통 시스템 SW** > **DB이미지** > **MariaDB(Ubuntu 16.04) on OpenStack** 메뉴를 클릭합니다.  

<br><br>

**3\) 이용 신청하러 가기**  

![service_detail](img/mariadb_service_detail.png)

- 페이지 하단의 **이용 신청하러 가기** 버튼을 클릭합니다.  

<br><br>

**4\) 이용 신청하기**  

![service_apply](img/mariadb_service_apply.png)

- **프로젝트** 선택 > **서비스 인스턴스 명** 입력 > **설명** 입력 > **서비스 플랜** 선택 > **서비스 파라미터** 입력 후 **이용신청** 버튼을 클릭하여 신청합니다.  

<br><br>

**5\) 내 서비스 생성 확인**  

![myService](img/mariadb_my_service.png)

- 상단 메뉴 중 **마이페이지** > **내 서비스 관리** 페이지에서 내 서비스가 생성됐는지 확인합니다.  

<br><br><br>



#### 1-2. 서비스의 가상머신에 유동아이피 연결
**1\) 콘솔로 이동**

![myService2](img/mariadb_my_service2.png)

- 상단 메뉴 중 **마이페이지** > **내 서비스 관리** 페이지에서 **콘솔로 이동** 버튼을 클릭합니다.  

<br><br>

**2\) 가상머신 생성 확인**

![consoleVmtable](img/mariadb_console_vmtable.png)

&nbsp;&nbsp; ① 콘솔 좌측 메뉴 중 **컴퓨트** > **가상머신** 을 클릭하여 가상머신 페이지로 이동합니다.  
&nbsp;&nbsp; ② 가상머신 페이지에서 내 가상머신 생성 여부를 확인합니다.  

<br><br>

**3\) 키 페어 저장**

- 내 프로젝트에서 **키 페어** 내 서비스 가상머신의 키 페어 명과 동일한 키 페어를 다운로드합니다.  

<br><br>

**4\) 유동 아이피 연결**

- 콘솔 좌측 메뉴 중 **네트워크** > **유동 아이피** 페이지에서 유동 아이피를 내 서비스 가상머신에 연결합니다.  

<br><br><br>

#### 1-3. 서비스 접속 및 DB 연결

**1\) 터미널로 서버 접속**  

```shell
# 키 페어를 통해 ubuntu 계정으로 서버 접속
ssh -i egovplatform.pem ubuntu@유동 아이피
```

<br>

&nbsp;&nbsp;&nbsp;&nbsp; MaraiDB Shell 접속  

```shell
# root 계정 및 패스워드로 mysql 쉘 접속
mysql -uroot -p비밀번호
```

<details>
<summary>실행 결과</summary>
<p>

```shell
root@mariadb-server:~# mysql -uroot -proot
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 155649
Server version: 10.4.12-MariaDB-log MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```
</p>
</details>

<br><br>

**2\) DB 연결**  

<br><br><br>

### 2. 서비스 상세 정보

- **파일 구조**

|      이름       |       설명        |
| :-------------: | :---------------: |
| 데이터 디렉토리 |  /var/lib/mysql   |
|  로그 디렉토리  | /var/log/mysql    |
|    설정 파일    | /etc/mysql/my.cnf |

<br>


- **추천 설정 파라미터 값**

|                  이름                  |                                                                            설명                                                                                   |
| :------------------------------------: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|         `expire_logs_days` = 7         | log-bin으로 설정된 로그크기가 커지는것을 방지하기 위해 7일간 저장하도록 합니다.                                                                                       |
|        `innodb_file_per_table`         | 테이블스페이스로 사용하는 경우 테이블 Drop시 이미 늘어난 물리 크기는 줄어들지 않기 때문에 테이블 삭제시 물리크기 확보를 위해 옵션을 사용합니다.                           |
|         `innodb_log_file_size`         | Heavy Traffic을 들어오는 경우 CheckPoint를 위한 병목을 방지하기 위해 innodb_log_file_size 옵션을 늘려주어 설정합니다.                                                 |
|      `innodb_log_files_in_group`       | innodb_log_file의 개수를 지정합니다. Rotation을 위해 2개 이상을 권고합니다.                                                                                         |
|        `default_storage_engine`        | Online-DDL및 Transction사용이 가능한 InnoDB로 표준 지정합니다.                                                                                                     |
| `query_cache_type`  `query_cache_size` | DB기동후 query_cache를 사용자가 동적으로 ON/OFF할수 있도록 ON / 0으로 설정합니다.                                                                                    |
|            `sysdate-is-now`            | sysdate를 사용하는 경우 Replication구성에서 Master-Slave간의 데이터처리 시간이 다를 경우 Time Type 데이터가 다를수 있어 sysdate의 경우 자동으로 now를 replace 해줍니다. |

<br>

- **MariaDB 기동** 

```shell
# mariadb 기동
sudo systemctl start mysqld
```

<br>

- **MariaDB 정지** 

```shell
# mariadb 정지
sudo systemctl stop mysqld
```

<br>

- **MariaDB 상태**

```shell
# mariadb 상태 확인
sudo systemctl status mysqld
```

<details>
<summary>실행 결과</summary>
<p>

```shell
root@mariadb-server:~# systemctl status mysqld
● mysqld.service - LSB: start and stop MariaDB
   Loaded: loaded (/etc/init.d/mysqld; bad; vendor preset: enabled)
   Active: active (running) since Wed 2020-08-26 08:14:54 UTC; 1 months 23 days ago
     Docs: man:systemd-sysv-generator(8)
    Tasks: 76
   Memory: 1.8G
      CPU: 3h 27min 24.621s
   CGroup: /system.slice/mysqld.service
           ├─2284 /bin/sh /usr/local/mysql/bin/mysqld_safe --datadir=/usr/local/mysql/data --pid-file=/usr/local/mysql/data/db-new.pid
           └─2443 /usr/local/mysql/bin/mysqld --basedir=/usr/local/mysql --datadir=/usr/local/mysql/data --plugin-dir=/usr/local/mysql/lib/pl

Aug 26 08:14:48 db-new systemd[1]: Starting LSB: start and stop MariaDB...
Aug 26 08:14:49 db-new mysqld[2194]: Starting MariaDB
Aug 26 08:14:49 db-new mysqld[2194]: .200826 08:14:49 mysqld_safe Logging to '/usr/local/mysql/data/db-new.err'.
Aug 26 08:14:49 db-new mysqld[2194]: 200826 08:14:49 mysqld_safe Starting mysqld daemon with databases from /usr/local/mysql/data
Aug 26 08:14:54 db-new mysqld[2194]: .... *
Aug 26 08:14:54 db-new systemd[1]: Started LSB: start and stop MariaDB.
```
</p>
</details>


<br>

- **MariaDB daemon 구동 유무**

```shell
# mariadb 구동 유무 확인
ps -ef | grep mysqld
```

<details>
<summary>실행 결과</summary>
<p>

```shell
root@mariadb-server:~# ps -ef | grep mysqld
root      2284     1  0 Aug26 ?        00:00:00 /bin/sh /usr/local/mysql/bin/mysqld_safe --datadir=/usr/local/mysql/data --pid-file=/usr/local/mysql/data/db-new.pid
mysql     2443  2284  0 Aug26 ?        03:27:03 /usr/local/mysql/bin/mysqld --basedir=/usr/local/mysql --datadir=/usr/local/mysql/data --plugin-dir=/usr/local/mysql/lib/plugin --user=mysql --log-error=/usr/local/mysql/data/db-new.err --pid-file=/usr/local/mysql/data/db-new.pid --socket=/tmp/mysql.sock --port=3306
```
</p>
</details>

<br>

- **root 비밀번호 변경**

```shell
# mysql 쉘 상에서 root 비밀번호 변경
mysql > set password = password('새 비밀번호');
```

<details>
<summary>실행 결과</summary>
<p>

```shell
MariaDB [(none)]> set password = password('root');
Query OK, 0 rows affected (0.073 sec)
```
</p>
</details>

<br>

- **DB 외부 접속 허용**  
 ① 설정 파일 편집  

```shell
# mariadb 설정 파일 편집
sudo vi /etc/mysql/my.cnf
```

<br>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; bind-address = 127.0.0.1 -> bind-address = 0.0.0.0 로 변경  

```shell
# bind-address 변경 전
bind-address = 127.0.0.1
```
  
```shell
# bind-address 변경 후
bind-address = 0.0.0.0
```

<br>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ② MariaDB 재시작  

```shell
# mariadb 재시작
sudo systemctl restart mysqld
```

<br>

---

## 필요 사항
- **DB 연결 시 에러 로그 `"Host 'X.X.X.X' is not allowed to connect to this MariaDB server."` 가 나올 경우 조치사항**  
DB가 설치된 가상머신에 터미널로 접속 후 명령어 입력

```shell
# root 계정으로 mysql 쉘 접속
mysql -uroot -p비밀번호

# mysql 쉘 상에서 호스트의 권한 변경 및 적용
mysql > grant all privileges on *.* to root@X.X.X.X identified by 'root 비밀번호' with grant option;
mysql > flush privileges;
```

<details>
<summary>실행 결과</summary>
<p>

```shell
root@mariadb-server:~# mysql -uroot -proot
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 10
Server version: 10.4.12-MariaDB-log MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> grant all privileges on *.* to root@10.10.200.214 identified by 'root' with grant option;
Query OK, 0 rows affected (0.020 sec)

MariaDB [(none)]> flush privileges;
Query OK, 0 rows affected (0.003 sec)
```
</p>
</details>

<br>

---

## 주의 사항
- **데이터 백업**  
--routines 옵션을 추가하지 않을 시, 사용자 프로시저 및 함수는 백업 되지 않고 데이터만 백업됩니다.

<details>
<summary>예제</summary>
<p>

```shell
root@mariadb-server:~# mysqldump -uroot -proot --routines 데이터베이스명 > 백업파일.sql
```
</p>
</details>

- **데이터 복원**  
데이터를 복원하기 전에 대상 데이터베이스를 만들어주어야 합니다.
 
<details>
<summary>예제</summary>
<p>

```shell
root@mariadb-server:~# mysql -uroot -proot 데이터베이스명 < 백업파일.sql 
```
</p>
</details>